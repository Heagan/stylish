models = []
areas = []
bodyTypes = []
colours = []

modfile = open("models","r")
for model in modfile:
    models.append(model.lower().split('\n')[0])

areafile = open("areas", "r")
for area in areafile:
    areas.append(area.lower().split('\n')[0])

bodfile = open("body types", "r")
for body in bodfile:
    bodyTypes.append(body.lower().split('\n')[0])

colfile = open("colours", "r")
for colour in colfile:
    colours.append(colour.lower().split('\n')[0])


class CarData:
    def __init__(self):
        self.model = ""
        self.area = ""
        self.body_type = ""
        self.condition = ""
        self.mileage = 0
        self.price = 0
        self.year = 0
        self.transmission = ""
        self.fuel_type = ""
        self.control = ""
        self.colour = ""
        self.keyword = []
        self.points = 0

    def print_info(self):
        print(self.model)
        print("R" + str(self.price))
        print(self.area)
        print(self.colour)

    def decode_info(self, description):
        print(description)
        for word in description.split():
            print(word)
            if word.lower() in models:
                self.model = word.lower()

            if word.lower() in colours:
                self.colour = word.lower()

            if word.lower() in areas:
                    self.area = word.lower()

            if word.lower().isnumeric():
                self.price = int(word)

    def fields_filled(self):
        fields = 1
        fields *= 10
        if len(self.model) > 0:
            fields += 1
        fields *= 10
        if self.price > 0:
            fields += 1
        fields *= 10
        if len(self.area) > 0:
            fields += 1
        fields *= 10
        if len(self.colour) > 0:
            fields += 1
        return fields

    def compare(self, c):
        similar_points = 0
        if self.model == c.model:
            similar_points += 4
        if self.colour == c.colour:
            similar_points += 2
        if self.area == c.area:
            similar_points += 2
        if self.price <= c.price:
            similar_points += 1
        return similar_points




