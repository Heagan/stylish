import car

conversation = []
mock_data = [car.CarData()]
car_info = car.CarData()
flag = True
get_car_info = True


def send(message):
    conversation.append(message)
    print(message)


def recieve():
    conversation.append(input())
    return conversation[-1]


def display_search_results():
    display_a = car.CarData()
    display_b = car.CarData()
    display_c = car.CarData()
    for mock_elem in mock_data:
        if mock_elem.points >= display_a.points:
            display_a = mock_elem
        elif mock_elem.points >= display_b.points:
            display_b = mock_elem
        elif mock_elem.points >= display_c.points:
            display_c = mock_elem
    send("Displaying Closest Search Results:\n")
    display_a.print_info()
    display_b.print_info()
    display_c.print_info()


def search_data(c):
    for mock_elem in mock_data:
        points = mock_elem.compare(c)
        mock_elem.points = points


def check_message(message):
    car_info.decode_info(message)


def show_dealer_info():
    send("Here's their number: 087 727 7420")
    send("Here's a link to the cars information:")
    send("cars.co.za/for-sale/used/1992-BMW-3-Series-316i-e36-Western-Cape-Cape-Town/3069130/")
    send("Would you like to search for another type of vehicle?")


def end_chat(c):
    search_data(c)
    display_search_results()
    send("Would you like to contact the dealer?")
    check_answer(recieve())


def ask_question(c):
    fields = list(str(c.fields_filled()))
    if fields[1] == "0":
        send("What car would you like?")
    elif fields[2] == "0":
        send("Whats your budget?")
    elif fields[3] == "0":
        send("Where do you live?")
    elif fields[4] == "0":
        send("What Colour?")
    else:
        send("Thats all the information we need!\nSearching for matching results!")
        end_chat(c)


def load_mock_data():
    mockfile = open("mockdata", "r")
    for mock in mockfile:
        c = car.CarData()
        c.decode_info(mock.lower())
        if len(c.model) > 0:
            mock_data.append(c)
    print("Mock Data: ")
    for mock in mock_data:
        mock.print_info()
    print("\n")


def check_answer(answer):
    # print(conversation)
    global car_info
    global flag
    if conversation[-2] == "Would you like to contact the dealer?":
        if answer == "no":
            send("Would you like to search for another type of vehicle?")
        else:
            show_dealer_info()
    elif conversation[-2] == "Would you like to search for another type of vehicle?":
        if answer != "no":
            car_info = car.CarData()
        else:
            flag = False
    else:
        check_message(user_response)


load_mock_data()
# ans = "bmw red 12331 gauteng"
# check_message(ans)

while flag:
    ask_question(car_info)
    user_response = recieve()
    if user_response == "q":
        break
    check_answer(user_response)
send("Have a lovely day!")
