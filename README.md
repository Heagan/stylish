# WhatsApp Chat Bot
<b>This was our teams submission to the Hackathon hosted by Cars.co.za in 2018</b>
<br><b>Our team of 3 were awarded second place</b>
<br><br>
The Hackathon was to create either a chat bot system or create a program that works with beacons for a <b>car sales company</b>
<br><br>
We chose to make a chat bot that works on any social media messaging system to allow users to ask for information about a vehicle they were looking to buy
They could enter some search criteria and have some choices show to them on WhatsApp for example, and if they wanted more information it would put them through with an assistant to help them more
<br><br>
The app would also keep track of information from any user and would be able to use previous information in any further communications with the user

# App Information
The app was written in python
<br>
It used NLTK(Natural Language Toolkit) to help with the conversations
<br>
And the Twilio API to chat with social messaging services

# To Run
The python script is just a matter of installing some packages through pip and running the main file, though I'm not sure how we start the Twilio API service as I didn't do that

# TO:DO
Add a requirements.txt to help getting it to run easier

# My Certificate

![](cert.png)









